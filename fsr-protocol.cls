\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fsr-protocol}[2019/04/30 FSR Protocol]

\DeclareOption*{\InputIfFileExists{\CurrentOption.min}{}{%
	\PassOptionsToClass{\CurrentOption}{scrartcl}}}
\ExecuteOptions{a4paper,12pt}
\ProcessOptions \relax

\LoadClass{scrartcl}

\RequirePackage[utf8]{inputenc}
\RequirePackage[ngerman]{babel}
\RequirePackage[T1]{fontenc}
\RequirePackage{eurosym}

\usepackage{csquotes}
\MakeOuterQuote{"}

\RequirePackage{graphicx}
\RequirePackage{xifthen}

\RequirePackage[hidelinks]{hyperref}

\RequirePackage{fsr-core}

\newcommand\longtop{Tagesordnungspunkt}


% already exists, overwrite
\def\@maketitle{%
	\newpage%
	\null%
	\vskip 2em%
	\begin{minipage}{.3\textwidth}%
		\includegraphics[width=.8\textwidth]{fsr-logo-igrey.png}%
	\end{minipage}%
	\begin{minipage}{.7\textwidth}%
		\begin{center}%
		\let\footnote\thanks%
			{\LARGE \fsri \par}%
			{\Large der \hszg \par}%
			\vskip 2mm%
			\ifthenelse%
				{\equal{\@title}{}}%
				{\LARGE Sitzung vom \@date}%
				{{\LARGE \@title \par} \@date \par}%
			\vskip 1.0em%
		\end{center}%
		
		\ifthenelse{\equal{\the\chairperson}{}}\relax{Sitzungsleitung: \the\chairperson\\}% EN: Chairperson
		Protokollführer:% EN: Clerk
		\begin{tabular}[t]{l}%
			\@author%
		\end{tabular}\par%
		%\vskip 1em%
		\small%
		Beginn der Sitzung: \the\Sstart\par% EN: Start of meeting
		Ende der Sitzung: \the\Send\par% EN: End of meeting
		Gewählte: \the\voted\par% EN: Voted
		\ifthenelse{\equal{\the\coopted}{}}\relax{Kooptierte: \the\coopted\par}% EN: Coopted
		\ifthenelse{\equal{\the\guests}{}}\relax{Gäste: \the\guests\par}%\the\voted% EN: Guests
		\quorate
		\vskip 1.5em%
	\end{minipage}%
	\vspace{2em}
}

\newcommand*\quorate{\@quorate}
\newcommand*\@quorate{}
\newcommand*\yesquorate{}
\def\yesquorate{%
	\def\@quorate{%
		\textbf{Der \FSR\ ist beschlussfähig.}% EN: The \FSR\ has a quorum.
		\par
	}%
}%
\newcommand*\nonquorate{\noquorate}
\newcommand*\noquorate{}
\def\noquorate{%
	\def\quorate{%
		\textbf{Der \FSR\ ist nicht beschlussfähig.}% EN: The \FSR\ does not have a quorum.
		\par
	}%
}%

\newcommand*\@theOldSec{}
\expandafter\def\expandafter\@theOldSec\expandafter{\thesection}
% use that optional argument advantage
\newcommand*\@modsec[1][TOP \arabic{section}]{%
	\expandafter\def\expandafter\@theOldSec\expandafter{\thesection}
	\renewcommand*{\thesection}{#1}%
	%\renewcommand*{\thesection}{Top \arabic{section}}%
}
\newcommand*\@restoresec{}
\def\@restoresec{%
	\expandafter\def\expandafter\thesection\expandafter{\@theOldSec}%
}

\newcommand*\oldtop{}
\expandafter\def\expandafter\oldtop\expandafter{\top}
% already defined, overwrite
\def\top#1{%
	\@ifnextchar[{\@top{#1}}{\@top{#1}[\relax]}%
}
\newcommand*\retop{}
\def\retop#1{%
	\@ifnextchar[{\@retop{#1}}{\@retop{#1}[\relax]}%
}
\newcommand*\@top{}
\def\@top#1[#2]{%
	\if\top@exists{#1}%
		\if\relax\noexpand#2\relax%
			\top@sec{\csname toplist@#1@num\endcsname}{}{\csname toplist@#1@name\endcsname}%
			\label{top:#1}%
		\else%
			\top@sec{\csname toplist@#1@num\endcsname}{}{#2}%
			\label{top:#1}%
		\fi%
	\else%
		\expandafter\def\csname toplist@#1@name\endcsname{#2}%
		\expandafter\def\csname toplist@#1@rec\endcsname{0}%
		\expandafter\expandafter\expandafter\def\expandafter\csname\expandafter t\expandafter o\expandafter p\expandafter l\expandafter i\expandafter s\expandafter t\expandafter @\expandafter #1\expandafter @\expandafter n\expandafter u\expandafter m\expandafter\endcsname\expandafter{\the\c@section}%
		\top@sec{\the\c@section}{}{#2}%
		\label{top:#1}%
		\global\advance\c@section by1%
	\fi%
}
\newcommand*\@retop{}
\def\@retop#1[#2]{%
	\if!\top@exists{#1}%
		\PackageError{fsr-protocol}{TOP to continue not found (#1)}{When continuing a TOP with \string\retop{A}, A must have already been created using \string\top{A}{title of A}.}%
	\else%
		%\def\csname toplist@#1@rec\endcsname{\numexpr\csname toplist@#1@rec\endcsname+1\relax}%
		\expandafter\@ddone\csname toplist@#1@rec\endcsname%
		\top@sec
			{\csname toplist@#1@num\endcsname}
			{{\small Fortsetzung%
				\ifnum\numexpr\csname toplist@#1@rec\endcsname-1>0\relax%
					\ \csname toplist@#1@rec\endcsname%
				\fi%
				:} TOP \arabic{section}
			}
			{\if\relax\noexpand#2\relax%
				\csname toplist@#1@name\endcsname%
			\else%
				#2%
			\fi}
		\ifnum\numexpr\csname toplist@#1@rec\endcsname-1>0\relax%
			\label{top:#1:\csname toplist@#1@rec\endcsname}%
		\else%
			\label{top:#1:\csname toplist@#1@rec\endcsname}%
		\fi%
	\fi%
}
\newcommand*\@ddone{}
\def\@ddone#1{%
	\expandafter\def\expandafter#1\expandafter{%
		\the\numexpr#1+1\relax%
	}
}
\newcommand*\top@exists{}
\def\top@exists#1{%
	\expandafter\if\expandafter\iscsundefined\expandafter{\csname toplist@#1@name\endcsname}%
		!\relax%
	\else%
		\relax\relax%
	\fi%
}

\newcommand*\top@sec{}
\def\top@sec#1#2#3{%
	\if\relax\noexpand#2\relax%
		\@modsec%
	\else%
		\@modsec[#2]%
	\fi%
	\expandafter\def\expandafter\tempc\expandafter{\the\c@section}%
	\setcounter{section}{#1}%
	\scr@startsection{section}{\csname sectionnumdepth\endcsname}{\csname scr@section@sectionindent\endcsname}{\csname scr@section@beforeskip\endcsname}{\csname scr@section@afterskip\endcsname}{\ifdim\glueexpr\csname scr@section@beforeskip\endcsname<\z@\expandafter\ifnum\scr@v@is@gt{2.96}\relax\setlength{\parfillskip}{\z@ plus 1fil}\fi\fi\raggedsection\normalfont\sectfont\nobreak\usekomafont{section}}{#3}%
	\setcounter{section}{\tempc}%
	\@restoresec%
}

\newcommand*\isundefined{}
\def\isundefined#1{TT\fi\ifx#1\undefined}
\newcommand*\iscsundefined{}
\def\iscsundefined#1{TT\fi\ifx#1\relax}


\newcommand\important[1]{%
	\par%
	\noindent%
	\vspace{.2em}%
	\fbox{%
		\begin{minipage}[t]{\dimexpr\linewidth-8pt}%
			\vspace{.3em}%
			#1
			\vspace{.3em}%
		\end{minipage}%
	}%
	\par%
	\vspace{.2em}%
}

\newcommand{\decision}[2]{%
	\important{%
		\textbf{%
			{\small Entscheidung:}\hspace{.6em}% EN: Decision
			#1%
		}%
		\ifthenelse
			{\equal{#2}{}}
			{}
			{\par%
				\vspace{.2em}%
				{\small Begründung:}\hspace{.6em}% EN: Reason
				#2%
			}
	}%
}

\newcommand{\vote}[4]{%
	\important{%
		\textbf{\small Abstimmung:}% EN: Vote
		\hspace{.6em}%\par%
		#1%
		\par%
		\hfill%
		\mbox{Dafür: #2 / Dagegen: #3 / Enthaltung: #4}% EN: For / Against / Abstention
		\par%
		\newcount\cntx%
		\cntx=#3%
		\advance\cntx by #4%
		\hfill%
		\mbox{%
			\ifnum#2>\cntx%
				Dem Antrag wurde stattgegeben.% EN: The proposal was granted.
				%($#2 > \the\cntx$)%
			\else%
				Der Antrag wurde abgelehnt.% EN: The proposal was rejected.
				%($#2 \leq \the\cntx$)%
			\fi%
		}%
	}%
}

\newcommand{\opinion}[3]{%
	\par%
	\noindent%
	\hspace{2pt}
	\begin{minipage}[t]{\dimexpr\linewidth-8pt}%
		\vspace{.3em}%
		\textbf{\small Meinungsbild:}% EN: Public opinion
		\hspace{.6em}%\par%
		#1%
		\par%
		\hfill%
		\mbox{Dafür: #2 / Dagegen: #3}% EN: For / Against
		\vspace{.3em}%
	\end{minipage}%
	\par%
	\vspace{.2em}%
}

\newcommand*\ev[1]{\par\textit{\small#1}}
\newcommand*\appear[1]{%
	\ev{%
		#1 erscheint zur Sitzung% EN: appears to the meeting
	}%
}
% plural workaround
\newcommand*\appearp[1]{%
	\ev{%
		#1 erscheinen zur Sitzung% EN: appear to the meeting
	}%
}
\newcommand*\disappear[1]{
	\ev{%
		#1 verlässt die Sitzung% EN: disappears from the meating
	}%
}
% plural workaround
\newcommand*\disappearp[1]{
	\ev{%
		#1 verlassen die Sitzung% EN: disappear from the meating
	}%
}

\newenvironment{interrupt} {%
  \par
  \addvspace{2em}%
  \noindent\hspace{2.5pt}\rule{2em}{.2pt}\hspace{\dimexpr-2.5pt-2em}%
  \vline width .8pt %
  \hspace{1.5pt}%
  \vline width .2pt %
  \hspace{2em}%
  \begin{minipage}[t]{\dimexpr\linewidth-5pt-4em}
  	\vspace{1em}%
} {%
  	\vspace{.5em}%
  \end{minipage}%
  \hspace{2em}%
  \vline width .2pt %
  \par
  \parskip0pt
  \nointerlineskip
  \noindent\hfill\rule{1em}{.2pt}\hspace*{2.5pt}%
  \par
  \addvspace{2em}%
}

%\newtoks\date
\newtoks\Sstart
\newtoks\Send

%\newtoks\members
\newtoks\voted
\newtoks\coopted
\newtoks\guests
\newtoks\chairperson

\date{\placeholderdate}
\Sstart{\placeholdertime}
\Send{\placeholdertime}

\title{}

\endinput



%	\newcommand{\setdate}{\date}
%
%	\newcommand{\@members}{}
%	\newcommand{\@guests}{}
%	\newcommand{\setmembers}[2]{\renewcommand{\@members}{#1}\renewcommand{\@guests}{#2}}
%	\newcommand{\showmembers}{\@members\@guests}
%	\newcommand{\showonlymembers}{\@members}
%
%	\newcommand{\@present}{}
%	\newcommand{\setpresent}[1]{\renewcommand{\@present}{#1}}
%	\newcommand{\showpresent}{\@present}
%
%	\newcommand{\appear}[1]{\hfill #1 appears}
%	\newcommand{\disappear}[1]{\hfill #1 disappears}
%
%	\newcommand{\@start}{}
%	\newcommand{\setstart}[1]{\renewcommand{\@start}{#1}}
%	\newcommand{\showstart}{\@start}
%
%	\newcommand{\@end}{}
%	\newcommand{\setend}[1]{\renewcommand{\@end}{#1}}
%	\newcommand{\showend}{\@end}
%
%	\newcommand{\decision}[2]{\fbox{\big Decision: #1\\\small Reason: #2}}
%
%	\newcommand{\vote}[4]{\fbox{\big Vote: #1\\Yes: #2 | No: #3 | (Abstention: #4)}}
%
%
%	%	% Functional foreach construct 
%	%	% #1 - Function to call on each comma-separated item in #3
%	%	% #2 - Parameter to pass to function in #1 as first parameter
%	%	% #3 - Comma-separated list of items to pass as second parameter to function #1
%	%	\def\foreach#1#2#3{%
%	%	  \@test@foreach{#1}{#2}#3,\@end@token%
%	%	}
%	%
%	%	% Internal helper function - Eats one input
%	%	\def\@swallow#1{}
%	%
%	%	% Internal helper function - Checks the next character after #1 and #2 and 
%	%	% continues loop iteration if \@end@token is not found 
%	%	\def\@test@foreach#1#2{%
%	%	  \@ifnextchar\@end@token%
%	%	    {\@swallow}%
%	%	    {\@foreach{#1}{#2}}%
%	%	}
%	%
%	%	% Internal helper function - Calls #1{#2}{#3} and recurses
%	%	% The magic of splitting the third parameter occurs in the pattern matching of the \def
%	%	\def\@foreach#1#2#3,#4\@end@token{%
%	%	  #1{#2}{#3}%
%	%	  \@test@foreach{#1}{#2}#4\@end@token%
%	%	}
%	%
%		\newcommand{\@createvar}[2]{
%			\expandafter\newcommand\csname myarrays@#1\endcsname{#2}%
%			\expandafter\newcommand\csname #1\endcsname[1]{\expandafter\renewcommand\csname myarrays@#1\endcsname{##1}}%
%		}
%	%
%	%	\newcommand{\@myarrays}{{@myarrays,1}}
%	%	\newcommand{\arrlength}[1]{
%	%		% return array length
%	%		\newcounter\i
%	%		\loop\ifnum\i<#1
%	%			\ifthenelse{\equal{#1}{}}
%	%			{}{}
%	%		\repeat
%	%	}
%	%	\newcommand{\arrget}[1]{
%	%		% get value at index #1 from array
%	%	}
%	%	\newcommand{\arrforearch}[2]{
%	%		% cycle through array
%	%	}
%	%	\newcommand{\arrcontains}[2]{
%	%		% check if a value exists within the array
%	%	}
%	%	\newcommand{\arrpush}[2]{
%	%		% append a value to the array
%	%	}
%	%	\newcommand{\arrpop}[1]{
%	%		% pop the last value from the array
%	%	}
%	%	\newcommand{\arrremove}[2]{
%	%		% remove a value from the array
%	%	}
%		\newcommand{\newarray}[2]{
%			% create new array
%			\providecommand#1
%			\renewcommand#1{#2}
%	%		\arrpush{myarrays}{{#1,0}}
%	%		\@createvar{#1}{#2}
%		}
%	%
%	%
%		\newarray{tops}
%		\renewcommand{\top}[2]{
%	%		\if\arrcontains{tops}{#1}
%	%			\big Continuation: #2\\
%	%			\label{top:#1-c}
%	%		\else
%	%			\arrpush{#1}
%				\section{TOP: #2}
%				\label{top:#1}
%	%		\fi
%		}
%
%	\newenvironment{interrupt}{}{}
%
%	%\DeclareOption{onecolumn}{\OptionNotUsed}
%	\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
%	\ProcessOptions\relax
