\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fsr-anlage}[2018/12/20 FSR Anlage]

\DeclareOption*{\InputIfFileExists{\CurrentOption.min}{}{%
	\PassOptionsToClass{\CurrentOption}{scrartcl}}}
\ExecuteOptions{a4paper,12pt}
\ProcessOptions \relax

\LoadClass{scrartcl}

\RequirePackage[utf8]{inputenc}
\RequirePackage[ngerman]{babel}
\RequirePackage[T1]{fontenc}
\RequirePackage{eurosym}

\RequirePackage{graphicx}
\RequirePackage{xifthen}

\RequirePackage[hidelinks,breaklinks]{hyperref}

\RequirePackage{fsr-core}

% already exists, overwrite
\def\@maketitle{%
	\newpage%
	\null%
	\begin{minipage}{.16\textwidth}%
		\includegraphics[width=.8\textwidth]{fsr-logo-igrey.png}%
	\end{minipage}%
	\begin{minipage}{.84\textwidth}%
		\begin{center}%
		%\let\footnote\thanks%
			{\fsri\ \small der \hszg \par}%
			\vskip 2mm%
			{\large Anlage \the\anlageX}\ {\small zur Sitzung vom \the\Sdate \par}%
			\ifthenelse{\equal{\@title}{}}\relax{\large \@title \par}%
			%\vskip 1.0em%
		\end{center}%
		{\flushright\small
			\ifthenelse{\equal{\@date}{}}\relax{\@date \par}%
			\ifthenelse{\equal{\@author}{}}\relax{
				Autor:% EN: Clerk
				\begin{tabular}[t]{l}%
					\@author%
				\end{tabular}\par
			}
		}%
		\vskip 1.5em%
	\end{minipage}%
	\vspace{2em}
}

\newcommand*\@theOldSec{}
\expandafter\def\expandafter\@theOldSec\expandafter{\thesection}
% use that optional argument advantage
\newcommand*\@modsec[1][Top \arabic{section}]{%
	\expandafter\def\expandafter\@theOldSec\expandafter{\thesection}
	\renewcommand*{\thesection}{#1}%
	%\renewcommand*{\thesection}{Top \arabic{section}}%
}
\newcommand*\@restoresec{}
\def\@restoresec{%
	\expandafter\def\expandafter\thesection\expandafter{\@theOldSec}%
}

\newcommand*\oldtop{}
\expandafter\def\expandafter\oldtop\expandafter{\top}
% already defined, overwrite
\def\top#1{%
	\@ifnextchar[{\@top{#1}}{\@top{#1}[\relax]}%
}
\newcommand*\retop{}
\def\retop#1{%
	\@ifnextchar[{\@retop{#1}}{\@retop{#1}[\relax]}%
}
\newcommand*\@top{}
\def\@top#1[#2]{%
	\if\top@exists{#1}%
		\if\relax\noexpand#2\relax%
			\top@sec{\csname toplist@#1@num\endcsname}{}{\csname toplist@#1@name\endcsname}%
			\label{top:#1}%
		\else%
			\top@sec{\csname toplist@#1@num\endcsname}{}{#2}%
			\label{top:#1}%
		\fi%
	\else%
		\expandafter\def\csname toplist@#1@name\endcsname{#2}%
		\expandafter\def\csname toplist@#1@rec\endcsname{0}%
		\expandafter\expandafter\expandafter\def\expandafter\csname\expandafter t\expandafter o\expandafter p\expandafter l\expandafter i\expandafter s\expandafter t\expandafter @\expandafter #1\expandafter @\expandafter n\expandafter u\expandafter m\expandafter\endcsname\expandafter{\the\c@section}%
		\top@sec{\the\c@section}{}{#2}%
		\label{top:#1}%
		\global\advance\c@section by1%
	\fi%
}
\newcommand*\@retop{}
\def\@retop#1[#2]{%
	\if!\top@exists{#1}%
		\PackageError{fsr-protocol}{Top to continue not found (#1)}{When continuing a TOP with \string\retop{A}, A must have already been created using \string\top{A}{title of A}.}%
	\else%
		%\def\csname toplist@#1@rec\endcsname{\numexpr\csname toplist@#1@rec\endcsname+1\relax}%
		\expandafter\@ddone\csname toplist@#1@rec\endcsname%
		\top@sec
			{\csname toplist@#1@num\endcsname}
			{{\small Fortsetzung%
				\ifnum\numexpr\csname toplist@#1@rec\endcsname-1>0\relax%
					\ \csname toplist@#1@rec\endcsname%
				\fi%
				:} Top \arabic{section}
			}
			{\if\relax\noexpand#2\relax%
				\csname toplist@#1@name\endcsname%
			\else%
				#2%
			\fi}
		\ifnum\numexpr\csname toplist@#1@rec\endcsname-1>0\relax%
			\label{top:#1:\csname toplist@#1@rec\endcsname}%
		\else%
			\label{top:#1:\csname toplist@#1@rec\endcsname}%
		\fi%
	\fi%
}
\newcommand*\@ddone{}
\def\@ddone#1{%
	\expandafter\def\expandafter#1\expandafter{%
		\the\numexpr#1+1\relax%
	}
}
\newcommand*\top@exists{}
\def\top@exists#1{%
	\expandafter\if\expandafter\iscsundefined\expandafter{\csname toplist@#1@name\endcsname}%
		!\relax%
	\else%
		\relax\relax%
	\fi%
}

\newcommand*\top@sec{}
\def\top@sec#1#2#3{%
	\if\relax\noexpand#2\relax%
		\@modsec%
	\else%
		\@modsec[#2]%
	\fi%
	\expandafter\def\expandafter\tempc\expandafter{\the\c@section}%
	\setcounter{section}{#1}%
	\scr@startsection{section}{\csname sectionnumdepth\endcsname}{\csname scr@section@sectionindent\endcsname}{\csname scr@section@beforeskip\endcsname}{\csname scr@section@afterskip\endcsname}{\ifdim\glueexpr\csname scr@section@beforeskip\endcsname<\z@\expandafter\ifnum\scr@v@is@gt{2.96}\relax\setlength{\parfillskip}{\z@ plus 1fil}\fi\fi\raggedsection\normalfont\sectfont\nobreak\usekomafont{section}}{#3}%
	\setcounter{section}{\tempc}%
	\@restoresec%
}

\newcommand*\isundefined{}
\def\isundefined#1{TT\fi\ifx#1\undefined}
\newcommand*\iscsundefined{}
\def\iscsundefined#1{TT\fi\ifx#1\relax}


\newcommand\important[1]{%
	\par%
	\noindent%
	\vspace{.2em}%
	\fbox{%
		\begin{minipage}[t]{\dimexpr\linewidth-8pt}%
			\vspace{.3em}%
			#1
			\vspace{.3em}%
		\end{minipage}%
	}%
	\par%
	\vspace{.2em}%
}

\newenvironment{interrupt} {%
  \par
  \addvspace{2em}%
  \noindent\hspace{2.5pt}\rule{2em}{.2pt}\hspace{\dimexpr-2.5pt-2em}%
  \vline width .8pt %
  \hspace{1.5pt}%
  \vline width .2pt %
  \hspace{2em}%
  \begin{minipage}[t]{\dimexpr\linewidth-5pt-4em}
  	\vspace{1em}%
} {%
  	\vspace{.5em}%
  \end{minipage}%
  \hspace{2em}%
  \vline width .2pt %
  \par
  \parskip0pt
  \nointerlineskip
  \noindent\hfill\rule{1em}{.2pt}\hspace*{2.5pt}%
  \par
  \addvspace{2em}%
}

\newtoks\Sdate
\newtoks\anlageX

\date{\placeholderdate}
\Sdate{\placeholderdate}

\title{}

\endinput
